//package com.example.demo.bootstrapp;
//
//import com.example.demo.model.Profile;
//import com.example.demo.model.User;
//import com.example.demo.repository.ProfileRepository;
//import com.example.demo.repository.UserRepository;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//
//@Component
//public class BootStrapData  implements CommandLineRunner {
//
//    private final UserRepository userRepository;
//    private final ProfileRepository profileRepository;
//
//    public BootStrapData(UserRepository userRepository, ProfileRepository profileRepository) {
//        this.userRepository = userRepository;
//        this.profileRepository = profileRepository;
//    }
//
//    @Override
//    public void run(String... args) throws Exception {
//        userRepository.deleteAllInBatch();
//        profileRepository.deleteAllInBatch();;
//
//        User sakis = new User("sakismaz", "sakis", "maznis", "wqeqw@ddsa", "dsds");
//        User kwstas = new User("kwstas", "kwsttas", "renis", "wfsf@rew", "dsds");
//        User takis = new User("takis", "takis1", "mgfd", "wqeqw@ddsa", "dsds");
//        Profile sakisProfile = new Profile("title not assign", "something description", "url null", "image null");
//        Profile kwstasProfile = new Profile("title not assign", "something description", "url null", "image null");
//        Profile takisProfile = new Profile("title not assign", "something description", "url null", "image null");
//
//        sakis.setProfile(sakisProfile);
//        sakisProfile.setUser(sakis);
//
//        kwstas.setProfile(kwstasProfile);
//        kwstasProfile.setUser(kwstas);
//        takis.setProfile(takisProfile);
//        takisProfile.setUser(takis);
//
//        userRepository.save(takis);
//        userRepository.save(kwstas);
//        userRepository.save(sakis);
//
//        sakis.getFollowing().add(takisProfile);
//        sakis.getFollowing().add(kwstasProfile);
//
//        sakisProfile.getFollowers().add(kwstas);
//
//        userRepository.save(takis);
//        userRepository.save(kwstas);
//        userRepository.save(sakis);
//
//    }
//}
